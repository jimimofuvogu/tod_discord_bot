import asyncio
import random
from typing import List, Set

import discord_components
from discord import Embed
from discord_components import Interaction, ActionRow
from redbot.core import commands
from redbot.core.bot import Red

from .buttons.buttonUtils import ButtonFactory
from .buttons.todButtons import TodButton, SkipButton


def make_message_task(name: str, bot: Red, user_id: int, timeout: int = None, check=None):
    if check is None:
        def _check(i: Interaction):
            return i.author.id == user_id
    else:
        def _check(i: Interaction):
            return i.author.id == user_id and check(i)
    return asyncio.create_task(bot.wait_for("message", timeout=timeout, check=_check), name=name)


def make_button_task(name: str, bot: Red, custom_id: str = None, timeout: int = None, check=None):
    if check is None:
        def _check(i: Interaction):
            return i.custom_id == custom_id
    else:
        def _check(i: Interaction):
            return i.custom_id == custom_id and check(i)
    return asyncio.create_task(bot.wait_for("button_click", timeout=timeout, check=_check), name=name)


def make_join_button_task(ctx: commands.Context, current_players: Set[int]):
    return make_button_task(name="join", custom_id="jn_btn", bot=ctx.bot,
                            check=lambda i: i.author.id not in current_players)


def make_leave_button_task(ctx: commands.Context, current_players: Set[int]):
    return make_button_task(name="leave", custom_id="bye_btn", bot=ctx.bot,
                            check=lambda i: i.author.id in current_players)


def make_truth_button_task(ctx: commands.Context, player_id: int):
    return make_button_task(name="truth", custom_id="truth_btn", bot=ctx.bot,
                            check=lambda i: i.author.id == player_id)


def make_dare_button_task(ctx: commands.Context, player_id: int):
    return make_button_task(name="dare", custom_id="dare_btn", bot=ctx.bot,
                            check=lambda i: i.author.id == player_id)


def make_check_button_task(ctx: commands.Context, player_id: int):
    return make_button_task(name="check", custom_id="check_btn", bot=ctx.bot,
                            check=lambda i: i.author.id == player_id)


def make_cross_button_task(ctx: commands.Context, player_id: int):
    return make_button_task(name="cross", custom_id="cross_btn", bot=ctx.bot,
                            check=lambda i: i.author.id == player_id)


async def await_tasks(tasks: List[asyncio.Task], return_when: str = asyncio.FIRST_COMPLETED, timeout: int = None) \
        -> asyncio.Task:
    done, pending = await asyncio.wait(tasks, return_when=return_when, timeout=timeout)
    for task in pending:
        try:
            task.cancel()
        except asyncio.CancelledError:
            pass
    return list(done)[0] if done else None


def shuffle(li: List) -> List:
    out: List = li.copy()
    le = len(li)
    for i in range(le - 1):
        j = random.randint(i + 1, le - 1)
        out[i], out[j] = out[j], out[i]
    return out


class Util:
    def __init__(self, ctx: commands.Context, color: int):
        self.ctx = ctx
        self.color = color

        button_factory = ButtonFactory(ctx)

        self.join_button: TodButton = button_factory.make_join_button()
        self.leave_button: TodButton = button_factory.make_leave_button()
        self.truth_button: TodButton = button_factory.make_truth_button()
        self.dare_button: TodButton = button_factory.make_dare_button()
        self.check_button: TodButton = button_factory.make_check_button()
        self.cross_button: TodButton = button_factory.make_fail_button()
        self.skip_button: SkipButton = button_factory.make_skip_button()

        # noinspection PyTypeChecker
        self.setup_buttons = ActionRow(self.join_button, self.leave_button)
        # noinspection PyTypeChecker
        self.new_bout_buttons = ActionRow(self.join_button, self.leave_button,
                                          self.truth_button, self.dare_button, self.skip_button)
        # noinspection PyTypeChecker
        self.ask_truth_buttons = ActionRow(self.join_button, self.leave_button, self.skip_button)
        # noinspection PyTypeChecker
        self.answered_truth_buttons = ActionRow(self.join_button, self.leave_button,
                                                self.check_button, self.cross_button, self.skip_button)
        # noinspection PyTypeChecker
        self.give_dare_buttons = ActionRow(self.join_button, self.leave_button, self.skip_button)
        # noinspection PyTypeChecker
        self.done_dare_buttons = ActionRow(self.join_button, self.leave_button,
                                           self.check_button, self.cross_button, self.skip_button)

    async def send(self, content: str = None, embed_title: str = None, embed_description: str = None,
                   components: List[discord_components.Component] = None):
        await self.ctx.send(content=content, embed=self.embed_of(embed_title, embed_description), components=components)

    def embed_of(self, t: str = None, d: str = None):
        if t is None and d is None:
            return None
        elif d is None:
            return Embed.from_dict({"title": t, "color": self.color})
        elif t is None:
            return Embed.from_dict({"description": d, "color": self.color})
        else:
            return Embed.from_dict({"title": t, "description": d, "color": self.color})

    def make_setup_tasks(self, tasked: int, tasker: int, players: Set[int]) -> List[asyncio.Task]:
        # noinspection PyUnresolvedReferences
        return [b.make_button_task(tasked=tasked, tasker=tasker, players=players)
                for b in self.setup_buttons.components]

    def make_new_bout_tasks(self, tasked: int, tasker: int, players: Set[int]) -> List[asyncio.Task]:
        # noinspection PyUnresolvedReferences
        return [b.make_button_task(tasked=tasked, tasker=tasker, players=players)
                for b in self.new_bout_buttons.components]

    def make_ask_truth_tasks(self, tasked: int, tasker: int, players: Set[int]) -> List[asyncio.Task]:
        # noinspection PyUnresolvedReferences
        t = [b.make_button_task(tasked=tasked, tasker=tasker, players=players)
             for b in self.ask_truth_buttons.components]
        t.append(make_message_task(name="truth_msg", user_id=tasker, bot=self.ctx.bot,
                                   check=lambda m: m.content.lower().startswith("truth:")))
        return t

    def make_answered_truth_tasks(self, tasked: int, tasker: int, players: Set[int]) -> List[asyncio.Task]:
        # noinspection PyUnresolvedReferences
        return [b.make_button_task(tasked=tasked, tasker=tasker, players=players)
                for b in self.answered_truth_buttons.components]

    def make_give_dare_tasks(self, tasked: int, tasker: int, players: Set[int]) -> List[asyncio.Task]:
        # noinspection PyUnresolvedReferences
        t = [b.make_button_task(tasked=tasked, tasker=tasker, players=players)
             for b in self.ask_truth_buttons.components]
        t.append(make_message_task(name="dare_msg", user_id=tasker, bot=self.ctx.bot,
                                   check=lambda m: m.content.lower().startswith("dare:")))
        return t

    def make_done_dare_tasks(self, tasked: int, tasker: int, players: Set[int]) -> List[asyncio.Task]:
        # noinspection PyUnresolvedReferences
        return [b.make_button_task(tasked=tasked, tasker=tasker, players=players)
                for b in self.done_dare_buttons.components]
