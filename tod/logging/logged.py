import logging.config

logging.basicConfig(format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")
logger = logging.getLogger("ToD")
logger.setLevel(logging.WARNING)


def logged(f):
    def wrapper(*args, **kwargs):
        logger.debug(f.__name__ + str(args) + str(kwargs))
        out = f(*args, **kwargs)
        logger.debug(f.__name__ + " -> " + str(out))
        return out
    return wrapper
