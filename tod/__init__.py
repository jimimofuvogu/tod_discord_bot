from threading import Lock
from typing import Dict, Union

import discord
from discord_components.client import DiscordComponents
from redbot.core import commands

from .TodGame import TodGame
from tod.logging.logged import logged
from .settings import settings


def setup(bot):
    bot.add_cog(MyCog(bot))


class MyCog(commands.Cog):

    # noinspection PyMissingConstructor
    def __init__(self, bot):
        self.bot = bot
        DiscordComponents(self.bot)
        self.games: Dict[int, TodGame] = dict()
        self.mutex = Lock()

    @commands.group(invoke_without_command=True)
    async def tod(self, ctx: commands.Context, color: str = None):
        self.mutex.acquire()
        c_id = ctx.channel.id
        if c_id in self.games:
            return

        if color is not None:
            try:
                col = int(color, 16) % 0x1000000
            except ValueError:
                await ctx.reply("Invalid Hex value for color")
                return
        else:
            col = (ctx.author.id % 0x505050) + 0x505050

        game: TodGame = TodGame(ctx, col)
        self.games[c_id] = game
        self.mutex.release()

        await game.start(ctx.author.id)

        self.mutex.acquire()
        self.games.pop(c_id)
        self.mutex.release()

    @tod.command()
    @commands.admin_or_permissions()
    async def emoji(self, ctx: commands.Context, name: str, emoji: Union[str, int]):
        if name not in settings:
            await ctx.reply(f"{name} is not in {', '.join(settings.keys())}")
        else:
            try:
                emoji = int(emoji)
                e = discord.utils.get(ctx.guild.emojis, id=emoji)
            except ValueError:
                e = emoji
            settings[name] = emoji
            await ctx.reply(f"{name} emoji changed to {e}")
