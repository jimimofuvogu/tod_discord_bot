import asyncio
import logging
import random
from typing import Set, Dict, List, Optional

import discord
from discord_components import ActionRow, Interaction
from redbot.core import commands

from tod.logging.logged import logged
from .tod_state import State
from .util import Util, await_tasks, shuffle

afk_time: int = 600


class TodGame:

    def __init__(self, ctx: commands.Context, color: int):
        self.ctx = ctx
        self.util: Util = Util(ctx, color)

        self.state: State = State.START
        self.players: Set = set()
        self.scores: Dict = dict()
        self.skips: Dict = dict()

        self.tasked: int = 0
        self.tasker: int = 0
        self.tasked_queue: List[int] = []
        self.tasker_queue: List[int] = []

        self.round: int = 0

    # noinspection PyBroadException
    @logged
    async def start(self, player: int):
        try:
            await self._run(self.ctx, player)
        except:
            logging.exception("")
            await self.util.send(embed_title="Game Crashed")

    @logged
    async def _run(self, ctx: commands.Context, player: int):
        self.state = await self.setup(player)

        while self.state is not State.EXIT:
            if self.should_game_end():
                break

            if self.state is State.NEW_ROUND:
                self.state = await self.new_round()
            elif self.state is State.NEW_BOUT:
                self.state = await self.new_bout()
            elif self.state is State.ASK_TRUTH:
                self.state = await self.ask_truth()
            elif self.state is State.ANSWER_TRUTH:
                self.state = await self.answer_truth()
            elif self.state is State.ANSWERED_TRUTH:
                self.state = await self.answered_truth()
            elif self.state is State.GIVE_DARE:
                self.state = await self.give_dare()
            elif self.state is State.DO_DARE:
                self.state = await self.do_dare()
            elif self.state is State.DONE_DARE:
                self.state = await self.done_dare()
            else:
                await ctx.send(f"state {self.state} not implemented yet")
                break

        if self.round > 0:
            await self.print_scores(self.scores.keys())
        await self.util.send(embed_title="Game Ended")

    @logged
    async def setup(self, player: int):
        # noinspection PyTypeChecker
        row: ActionRow = self.util.setup_buttons
        await self.util.send(embed_title="A New Game Has Started", components=[row])
        p: discord.Member = self.ctx.guild.get_member(player)
        await self.join_handler(p)

        running: bool = True
        while running:
            t: List[asyncio.Task] = self.util.make_setup_tasks(
                tasked=self.tasked, tasker=self.tasker, players=self.players)
            action: asyncio.Task = await await_tasks(t, timeout=10)
            if not action:
                break
            state = await self.menu_pick_handler(action)
            if state is not None:
                return state

        if self.should_game_end():
            return State.EXIT
        else:
            return State.NEW_ROUND

    @logged
    async def new_round(self) -> State:
        self.round += 1
        await self.util.send(embed_title=f"Round {self.round}")
        self.tasked_queue = random.sample(list(self.players), len(self.players))
        self.tasker_queue = shuffle(self.tasked_queue)
        return State.NEW_BOUT

    @logged
    async def new_bout(self) -> State:
        if len(self.tasked_queue) == 0:
            return State.NEW_ROUND

        self.tasked = self.tasked_queue.pop()
        self.tasker = self.pick_next_tasker()
        member = self.ctx.guild.get_member(self.tasked)

        # noinspection PyTypeChecker
        row: ActionRow = self.util.new_bout_buttons
        await self.util.send(content=member.mention, embed_title="Please Pick Truth/Dare", components=[row])

        while True:
            t: List[asyncio.Task] = self.util.make_new_bout_tasks(
                tasked=self.tasked, tasker=self.tasker, players=self.players)
            action: asyncio.Task = await await_tasks(t, timeout=afk_time)
            if not action:
                return State.EXIT
            state = await self.menu_pick_handler(action)
            if state is not None:
                return state

    @logged
    async def ask_truth(self) -> State:
        tasker: discord.Member = self.ctx.guild.get_member(self.tasker)
        tasked: discord.Member = self.ctx.guild.get_member(self.tasked)

        # noinspection PyTypeChecker
        row: ActionRow = self.util.ask_truth_buttons
        await self.util.send(content=tasker.mention,
                             embed_title=f"Please Ask {tasked.display_name} A Truth",
                             embed_description='To ask a question write "truth: " and then your question.',
                             components=[row])

        while True:
            t: List[asyncio.Task] = self.util.make_ask_truth_tasks(
                tasked=self.tasked, tasker=self.tasker, players=self.players)
            action: asyncio.Task = await await_tasks(t, timeout=afk_time)
            if not action:
                return State.EXIT
            state = await self.menu_pick_handler(action, tasker=tasker, tasked=tasked)
            if state is not None:
                return state

    # noinspection PyMethodMayBeStatic
    @logged
    async def answer_truth(self) -> State:
        return State.ANSWERED_TRUTH

    async def answered_truth(self) -> State:
        tasker: discord.Member = self.ctx.guild.get_member(self.tasker)
        tasked: discord.Member = self.ctx.guild.get_member(self.tasked)

        # noinspection PyTypeChecker
        row: ActionRow = self.util.answered_truth_buttons
        await self.util.send(content=tasker.mention,
                             embed_description=f'Please click the corresponding button when {tasked.display_name} has '
                                               f'answered your question.',
                             components=[row])

        while True:
            t: List[asyncio.Task] = self.util.make_answered_truth_tasks(
                tasked=self.tasked, tasker=self.tasker, players=self.players)
            action: asyncio.Task = await await_tasks(t, timeout=afk_time)
            if not action:
                return State.EXIT
            state = await self.menu_pick_handler(action)
            if state is not None:
                return state

    @logged
    async def give_dare(self) -> State:
        tasker: discord.Member = self.ctx.guild.get_member(self.tasker)
        tasked: discord.Member = self.ctx.guild.get_member(self.tasked)

        # noinspection PyTypeChecker
        row: ActionRow = self.util.give_dare_buttons
        await self.util.send(content=tasker.mention,
                             embed_title=f"Please Give {tasked.display_name} A Dare",
                             embed_description='To give a dare write "dare: " and then your dare.',
                             components=[row])

        while True:
            t: List[asyncio.Task] = self.util.make_give_dare_tasks(
                tasked=self.tasked, tasker=self.tasker, players=self.players
            )
            action: asyncio.Task = await await_tasks(t, timeout=afk_time)
            if not action:
                return State.EXIT
            state = await self.menu_pick_handler(action, tasker=tasker, tasked=tasked)
            if state is not None:
                return state

    # noinspection PyMethodMayBeStatic
    @logged
    async def do_dare(self) -> State:
        return State.DONE_DARE

    @logged
    async def done_dare(self) -> State:
        tasker: discord.Member = self.ctx.guild.get_member(self.tasker)
        tasked: discord.Member = self.ctx.guild.get_member(self.tasked)

        # noinspection PyTypeChecker
        row: ActionRow = self.util.done_dare_buttons
        await self.util.send(content=tasker.mention,
                             embed_description=f'Please click the corresponding button when {tasked.display_name} has '
                                               f'done your dare.',
                             components=[row])

        while True:
            t: List[asyncio.Task] = self.util.make_done_dare_tasks(
                tasked=self.tasked, tasker=self.tasker, players=self.players)
            action: asyncio.Task = await await_tasks(t, timeout=afk_time)
            if not action:
                return State.EXIT
            state = await self.menu_pick_handler(action)
            if state is not None:
                return state

    @logged
    def should_game_end(self) -> bool:
        return len(self.players) < 2

    @logged
    async def menu_pick_handler(self, finished_task: asyncio.Task,
                                tasker: discord.Member = None, tasked: discord.Member = None) \
            -> Optional[State]:
        action: str = finished_task.get_name()
        interaction = finished_task.result()
        if isinstance(interaction, Interaction):
            await interaction.defer(edit_origin=True)

        if action == "join":
            await self.join_handler(interaction.author)
        elif action == "leave":
            s: State = await self.leave_handler(interaction.author)
            if s is not None:
                return s
            if self.should_game_end():
                return State.EXIT
        elif action == "truth":
            return State.ASK_TRUTH
        elif action == "dare":
            return State.GIVE_DARE
        elif action == "check":
            await self.check_handler()
            return State.NEW_BOUT
        elif action == "fail":
            await self.cross_handler()
            return State.NEW_BOUT
        elif action == "dare_msg":
            await self.util.send(content=tasked.mention,
                                 embed_title=f"{tasker.display_name} gave {tasked.display_name} the dare:",
                                 embed_description=interaction.content[5:].strip(" "))
            return State.DO_DARE
        elif action == "truth_msg":
            await self.util.send(content=tasked.mention,
                                 embed_title=f"{tasker.display_name} asked {tasked.display_name} for the truth:",
                                 embed_description=interaction.content[6:].strip(" "))
            return State.ANSWER_TRUTH
        elif action == "skip":
            s: State = await self.skip_handler()
            if s is not None:
                return s
        return None

    @logged
    async def leave_handler(self, m: discord.Member) -> Optional[State]:
        p_id = m.id
        self.players.remove(p_id)

        await self.util.send(embed_description=f"Bye {m.mention}")

        if p_id in self.tasked_queue:
            self.tasked_queue.remove(p_id)
        if p_id in self.tasker_queue:
            self.tasker_queue.remove(p_id)

        if p_id == self.tasker:
            self.tasker = self.pick_next_tasker()
            # if in these relevant stages, restart it with the new tasker
            if self.state in [State.GIVE_DARE, State.DONE_DARE, State.ASK_TRUTH, State.ANSWERED_TRUTH]:
                return self.state
        elif p_id == self.tasked:
            return State.NEW_BOUT
        else:
            return None

    @logged
    async def join_handler(self, m: discord.Member):
        p_id = m.id
        self.players.add(p_id)
        await self.util.send(embed_description=f"Hi {m.mention}")
        if p_id not in self.scores.keys():
            self.scores[p_id] = 0

    @logged
    async def skip_handler(self) -> Optional[State]:
        await self.util.send(embed_title="Skipped")
        if self.state in [State.GIVE_DARE, State.DONE_DARE, State.ASK_TRUTH, State.ANSWERED_TRUTH]:
            tasker: discord.Member = self.ctx.guild.get_member(self.tasker)
            return await self.leave_handler(tasker)
        else:
            tasked: discord.Member = self.ctx.guild.get_member(self.tasked)
            return await self.leave_handler(tasked)

    @logged
    async def check_handler(self):
        await self.util.send(embed_title="Task Completed")
        self.scores[self.tasked] += 1
        await self.print_scores(self.players)

    async def cross_handler(self):
        await self.util.send(embed_title="Task Failed")
        self.scores[self.tasked] -= 1
        await self.print_scores(self.players)

    @logged
    async def print_scores(self, players):
        sc = [(p, self.scores[p]) for p in players]
        sc.sort(key=lambda k: k[1], reverse=True)
        msg = "\n".join([f"{self.ctx.guild.get_member(p_id).mention} {score}" for p_id, score in sc])
        await self.util.send(embed_title=f"Round {self.round}", embed_description=msg)

    @logged
    def pick_next_tasker(self) -> int:
        q_empty = len(self.tasker_queue) == 0
        tasker_is_tasked = len(self.tasker_queue) == 1 and self.tasker_queue[0] == self.tasked
        if q_empty or tasker_is_tasked:
            pickable = list(self.players)
            if self.tasked in pickable:
                pickable.remove(self.tasked)
            return random.choice(pickable) if pickable else self.tasked
        else:
            t = self.tasker_queue.pop()
            while t == self.tasked:
                self.tasker_queue.insert(0, t)
                t = self.tasker_queue.pop()
            return t
