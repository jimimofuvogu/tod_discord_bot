from typing import Union

import discord
from redbot.core import commands

from tod.logging.logged import logged
from tod.buttons.todButtons import TodButton, JoinButton, LeaveButton, SkipButton, FailButton, CheckButton, \
    DareButton, TruthButton
from tod.settings import settings


class ButtonFactory:
    def __init__(self, ctx: commands.Context):
        self.ctx = ctx

    @logged
    def make_emoji(self, emoji: Union[str, discord.Emoji]):
        if not isinstance(emoji, str):
            return discord.utils.get(self.ctx.guild.emojis, id=emoji)
        else:
            return emoji

    def make_join_button(self) -> TodButton:
        emoji = self.make_emoji(settings["join"])
        return JoinButton(emoji=emoji, style=1, ctx=self.ctx)

    def make_leave_button(self) -> TodButton:
        emoji = self.make_emoji(settings["leave"])
        return LeaveButton(emoji=emoji, style=1, ctx=self.ctx)

    def make_truth_button(self) -> TodButton:
        emoji = self.make_emoji(settings["truth"])
        return TruthButton(emoji=emoji, style=2, ctx=self.ctx)

    def make_dare_button(self) -> TodButton:
        emoji = self.make_emoji(settings["dare"])
        return DareButton(emoji=emoji, style=2, ctx=self.ctx)

    def make_check_button(self) -> TodButton:
        emoji = self.make_emoji(settings["check"])
        return CheckButton(emoji=emoji, style=3, ctx=self.ctx)

    def make_fail_button(self) -> TodButton:
        emoji = self.make_emoji(settings["fail"])
        return FailButton(emoji=emoji, style=4, ctx=self.ctx)

    def make_skip_button(self) -> SkipButton:
        emoji = self.make_emoji(settings["skip"])
        return SkipButton(emoji=emoji, style=2, ctx=self.ctx)
