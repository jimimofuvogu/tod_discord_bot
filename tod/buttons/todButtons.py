import asyncio
import logging
from abc import abstractmethod
from typing import Union, Set

from discord import Emoji
from discord_components import Interaction, Button
from redbot.core import commands
from redbot.core.bot import Red

from tod.logging.logged import logged


class TodButton(Button):
    def __init__(self, label: str, emoji: Union[str, Emoji], custom_id: str, style: int,
                 action_name: str, ctx: commands.Context):
        custom_id += "_" + str(ctx.channel.id)
        super().__init__(label=label, style=style, custom_id=custom_id, emoji=emoji)
        self.action_name: str = action_name
        self.bot: Red = ctx.bot

    @abstractmethod
    def make_button_task(self, tasked: int, tasker: int, players: Set[int]) -> asyncio.Task:
        pass

    def _make_button_task(self, whitelist: Set[int] = None, blacklist: Set[int] = None) -> asyncio.Task:
        @logged
        def _check(i: Interaction):
            p_id = i.author.id
            w = whitelist is None or p_id in whitelist
            b = blacklist is None or p_id not in blacklist
            return i.custom_id == self._id and w and b
        return asyncio.create_task(self.bot.wait_for("button_click", check=_check), name=self.action_name)


class JoinButton(TodButton):
    def make_button_task(self, tasked: int, tasker: int, players: Set[int]) -> asyncio.Task:
        return super()._make_button_task(blacklist=players)

    def __init__(self, emoji: Union[str, Emoji], style: int, ctx: commands.Context):
        super().__init__(label="Join", style=style, custom_id="jn_btn", emoji=emoji, action_name="join", ctx=ctx)


class LeaveButton(TodButton):
    def make_button_task(self, tasked: int, tasker: int, players: Set[int]) -> asyncio.Task:
        return super()._make_button_task(whitelist=players)

    def __init__(self, emoji: Union[str, Emoji], style: int, ctx: commands.Context):
        super().__init__(label="Leave", style=style, custom_id="lv_btn", emoji=emoji, action_name="leave", ctx=ctx)


class TruthButton(TodButton):
    def make_button_task(self, tasked: int, tasker: int, players: Set[int]) -> asyncio.Task:
        return super()._make_button_task(whitelist={tasked})

    def __init__(self, emoji: Union[str, Emoji], style: int, ctx: commands.Context):
        super().__init__(label="Truth", style=style, custom_id="tr_btn", emoji=emoji, action_name="truth", ctx=ctx)


class DareButton(TodButton):
    def make_button_task(self, tasked: int, tasker: int, players: Set[int]) -> asyncio.Task:
        return super()._make_button_task(whitelist={tasked})

    def __init__(self, emoji: Union[str, Emoji], style: int, ctx: commands.Context):
        super().__init__(label="Dare", style=style, custom_id="dr_btn", emoji=emoji, action_name="dare", ctx=ctx)


class CheckButton(TodButton):
    def make_button_task(self, tasked: int, tasker: int, players: Set[int]) -> asyncio.Task:
        return super()._make_button_task(whitelist={tasker})

    def __init__(self, emoji: Union[str, Emoji], style: int, ctx: commands.Context):
        super().__init__(label="Completed", style=style, custom_id="chk_btn", emoji=emoji, action_name="check", ctx=ctx)


class FailButton(TodButton):
    def make_button_task(self, tasked: int, tasker: int, players: Set[int]) -> asyncio.Task:
        return super()._make_button_task(whitelist={tasker})

    def __init__(self, emoji: Union[str, Emoji], style: int, ctx: commands.Context):
        super().__init__(label="Failed", style=style, custom_id="fl_btn", emoji=emoji, action_name="fail", ctx=ctx)


class SkipButton(TodButton):
    def __init__(self, emoji: Union[str, Emoji], style: int, ctx: commands.Context):
        super().__init__(label="Skip", style=style, custom_id="skp_btn", emoji=emoji, action_name="skip", ctx=ctx)

    def make_button_task(self, tasked: int, tasker: int, players: Set[int]):
        skipper = self.Skipper(len(players) // 2)

        @logged
        def _check(i: Interaction):
            p_id = i.author.id
            whitelisted = p_id in players
            return i.custom_id == self._id and whitelisted and skipper.check(i)
        return asyncio.create_task(self.bot.wait_for("button_click", check=_check), name=self.action_name)

    class Skipper:
        def __init__(self, num: int):
            self.skiplist: Set[int] = set()
            self.num = num

        @logged
        def check(self, i: Interaction):
            p_id: int = i.author.id
            self.skiplist.add(p_id)
            i.component.label = f"Skip {len(self.skiplist)}/{self.num}"
            logging.debug(f"Skipping {len(self.skiplist)}/{self.num}")
            return len(self.skiplist) >= self.num
